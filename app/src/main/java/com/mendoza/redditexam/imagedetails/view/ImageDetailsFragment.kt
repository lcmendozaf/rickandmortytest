package com.mendoza.redditexam.imagedetails.view

import android.graphics.Bitmap
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mendoza.redditexam.R
import com.mendoza.redditexam.databinding.FragmentImageDetailsBinding
import com.mendoza.redditexam.databinding.FragmentImageListBinding
import com.mendoza.redditexam.imagelist.datasource.entity.Result
import com.squareup.picasso.Picasso

/**
 * A simple [Fragment] subclass.
 * Use the [ImageDetailsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ImageDetailsFragment : Fragment() {
    private lateinit var binding: FragmentImageDetailsBinding
    private lateinit var image:Result

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            image = it.getSerializable("image") as Result
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentImageDetailsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Picasso.get()
            .load(image.image)
            .placeholder(R.drawable.progress_animation)
            .error(android.R.drawable.gallery_thumb)
            .config(Bitmap.Config.ARGB_8888)
            .into(binding.imageView2)

        //Not enough time to make it pretty...
        binding.tvName.text = image.name
        binding.tvStatus.text = image.status
        binding.tvOrigin.text = image.origin.toString()
    }
}