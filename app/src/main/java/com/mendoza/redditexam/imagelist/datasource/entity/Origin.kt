package com.mendoza.redditexam.imagelist.datasource.entity

data class Origin(
    val name: String?,
    val url: String?
) {
    override fun toString(): String {
        return name ?:"-"
    }
}