package com.mendoza.redditexam.imagelist.view.adapter

import android.graphics.Bitmap
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.text.HtmlCompat
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.RecyclerView
import com.mendoza.redditexam.R
import com.mendoza.redditexam.databinding.ImageViewItemBinding
import com.mendoza.redditexam.imagelist.datasource.entity.Result
import com.squareup.picasso.Picasso

class ImageListAdapter(val onImageClicked:(Result)->Unit): PagingDataAdapter<Result, ImageListAdapter.ImageViewHolder>(Result.DIFF_CALLBACK) {

    class ImageViewHolder(val binding:ImageViewItemBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder {
        return ImageViewHolder(
            ImageViewItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        with(getItem(position)) {
            Log.d(ImageListAdapter::class.java.name, "Image URL "+this?.image)
            Picasso.get()
                .load(this?.image)
                .placeholder(R.drawable.progress_animation)
                .error(android.R.drawable.gallery_thumb)
                .config(Bitmap.Config.ARGB_8888)
                .into(holder.binding.imageView)
            holder.binding.tvDescription.text = HtmlCompat.fromHtml("<h1>${this?.name}</h1><br/><b>Gender </b>${this?.gender}<br/><b>Type</b> ${this?.type ?: "-"}<br/><b>Status</b> ${this?.status}",HtmlCompat.FROM_HTML_MODE_COMPACT)

            holder.binding.root.setOnClickListener {
                onImageClicked(this!!)
            }
        }
    }
}