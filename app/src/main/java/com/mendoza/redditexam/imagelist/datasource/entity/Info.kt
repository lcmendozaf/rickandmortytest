package com.mendoza.redditexam.imagelist.datasource.entity

data class Info(
    val count: Int?,
    val next: String?,
    val pages: Int?,
    val prev: String?
)