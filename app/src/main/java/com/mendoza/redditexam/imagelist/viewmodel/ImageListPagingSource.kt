package com.mendoza.redditexam.imagelist.viewmodel

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.mendoza.redditexam.imagelist.datasource.entity.Result
import com.mendoza.redditexam.imagelist.datasource.service.RickAndMortyService
import retrofit2.HttpException
import java.io.IOException

class ImageListPagingSource(private val redditService: RickAndMortyService): PagingSource<String, Result>() {
    override fun getRefreshKey(state: PagingState<String, Result>): String? {
        return state.anchorPosition?.let { anchorPosition ->
            state.closestPageToPosition(anchorPosition)?.nextKey
        }
    }

    override suspend fun load(params: LoadParams<String>): LoadResult<String, Result> {
        return try {
            val response = redditService.getPage(params.key ?: "1")
            val prev = response.info?.prev?.subSequence(response.info.prev.lastIndexOf("=") + 1, response.info.prev.length)
            val next = response.info?.next?.subSequence(response.info.next.lastIndexOf("=") + 1, response.info.next.length)
            LoadResult.Page(
                response.results ?: listOf(),
                prev,
                next
            ) as LoadResult<String, Result>
        } catch (exception: IOException) {
            LoadResult.Error(exception)
        } catch (exception: HttpException) {
            LoadResult.Error(exception)
        }
    }
}