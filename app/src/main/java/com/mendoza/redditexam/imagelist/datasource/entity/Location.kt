package com.mendoza.redditexam.imagelist.datasource.entity

data class Location(
    val name: String?,
    val url: String?
)