package com.mendoza.redditexam.imagelist.datasource.entity

data class ServiceResponse(
    val info: Info?,
    val results: List<Result>?
)