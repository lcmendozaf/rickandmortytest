package com.mendoza.redditexam.imagelist.datasource.service

import com.mendoza.redditexam.imagelist.datasource.entity.ServiceResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface RickAndMortyService {
    @GET("/api/character/")
    suspend fun getPage(@Query("page") page:String): ServiceResponse
}