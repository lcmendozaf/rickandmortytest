package com.mendoza.redditexam.imagelist.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.mendoza.redditexam.RedditClient
import com.mendoza.redditexam.imagelist.datasource.service.RickAndMortyService
import kotlinx.coroutines.flow.Flow
import com.mendoza.redditexam.imagelist.datasource.entity.Result

class ImageListViewModel:ViewModel() {

    fun fetchImages() : Flow<PagingData<Result>> {
        return getRepositoryStream()
    }

    private fun getRepositoryStream(): Flow<PagingData<Result>> {
        return Pager(
            config = PagingConfig(10,
                enablePlaceholders = true),
            pagingSourceFactory = { ImageListPagingSource(RedditClient.getRetrofit().create(RickAndMortyService::class.java)) }
        ).flow
    }
}