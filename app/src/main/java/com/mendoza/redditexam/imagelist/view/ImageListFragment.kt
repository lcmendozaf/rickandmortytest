package com.mendoza.redditexam.imagelist.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mendoza.redditexam.R
import com.mendoza.redditexam.databinding.FragmentImageListBinding
import com.mendoza.redditexam.imagelist.view.adapter.ImageListAdapter
import com.mendoza.redditexam.imagelist.viewmodel.ImageListViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import com.mendoza.redditexam.imagelist.datasource.entity.Result

/**
 *
 */
class ImageListFragment : Fragment() {

    private lateinit var binding: FragmentImageListBinding
    private val viewModel: ImageListViewModel by viewModels()
//Directions would've been better for navigation


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentImageListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initRecyclerView()
        setupObservers()
        initSwipeToRefresh()

        fetchNewImages()
    }

    private fun initSwipeToRefresh() {
        binding.swipeRefresh.setOnRefreshListener {
            fetchNewImages()
        }
    }

    private fun fetchNewImages() {
        binding.swipeRefresh.isRefreshing = true

        lifecycleScope.launch {
            viewModel.fetchImages()
                .collectLatest {
                    (binding.imageList.adapter as ImageListAdapter).submitData(it)
                }
        }
    }

    private fun setupObservers() {
        lifecycleScope.launch {
            (binding.imageList.adapter as ImageListAdapter).loadStateFlow.collectLatest { loadStates ->
                binding.swipeRefresh.isRefreshing = loadStates.refresh is LoadState.Loading
                binding.emptyText.visibility = if(loadStates.refresh !is LoadState.Loading && loadStates.refresh !is LoadState.Error && binding.imageList.adapter?.itemCount == 0) {
                    View.VISIBLE
                } else
                    View.GONE
            }
        }
    }

    private fun initRecyclerView() {
        binding.imageList.layoutManager = GridLayoutManager(context, 3, RecyclerView.VERTICAL, false)
        binding.imageList.adapter = ImageListAdapter(this::onImageClicked)
    }

    private fun onImageClicked(image:Result) {
        findNavController().navigate(R.id.actionOpenDetails, Bundle().apply {
            putSerializable("image",image)
        })
    }
}