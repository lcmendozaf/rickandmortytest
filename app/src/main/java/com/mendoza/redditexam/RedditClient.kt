package com.mendoza.redditexam

import androidx.viewbinding.BuildConfig
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RedditClient {

    companion object {
        private const val BASE_URL = "https://rickandmortyapi.com/"
        private var retrofit: Retrofit? = null

        fun getRetrofit():Retrofit {
            return retrofit ?:
            Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(buildRetrofitClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build().also {
                    retrofit = it
                }
        }

        private fun buildRetrofitClient():OkHttpClient {
            val builder = OkHttpClient.Builder()
            return builder.build()
        }
    }
}